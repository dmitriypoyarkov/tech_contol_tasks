from matplotlib.lines import Line2D
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import math
from matplotlib.animation import FuncAnimation


def plot_freq(freq):
    plot_explanation.set_text('Фаза выходного почти не меняется\nс ростом частоты входного')
    return output_plot(1, 0.05 * freq + 0.1)


def plot_x0(x0):
    plot_explanation.set_text('Наблюдаем линейную зависимость от входной амплитуды')
    return output_plot(x0, 1)


def output_plot(x0, freq):
    global system, lines, plot_text, plot_explanation
    t_input = np.linspace(0, 10, 1001)
    input_signal = [x0 * math.sin(item * 2 * math.pi * freq) for item in t_input]
    t_out, y_out, x_out = signal.lsim(system, input_signal, t_input)
    lines[0].set_data(t_input, input_signal)
    lines[1].set_data(t_out, y_out)
    plot_text.set_text('freq: ' + str(freq) + '\nmag: ' + str(x0))
    
    return lines + [plot_text] + [plot_legend]


if __name__ == '__main__':
    a = 0.1
    c = 0.01
    transmission_function = [[a, 1], [c, 0, 1]]
    system = signal.lti(*transmission_function)
    
    fig = plt.figure()
    ax = plt.axes(xlim=(0, 10), ylim=(-100, 100))
    lines = [plt.plot([], [])[0] for _ in range(2)]
    plot_text = plt.text(0.1,75,'')
    plot_explanation = plt.title('')
    plot_legend = plt.legend(['input', 'output'])
    magnitude_anim = FuncAnimation(fig, plot_x0,
                               frames=20, interval=100, blit=True)
    plt.show()
    
    fig1 = plt.figure()
    ax = plt.axes(xlim=(0, 10), ylim=(-40, 40))
    lines = [plt.plot([], [])[0] for _ in range(2)]
    plot_text = plt.text(0.1,25,'')
    plot_explanation = plt.title('')
    plot_legend = plt.legend(['input', 'output'])
    frequency_anim = FuncAnimation(fig1, plot_freq,
                               frames=60, interval=100, blit=True)
    plt.show()

    # experimental
    w = np.arange(0, 10**2, 0.01)
    cycle_frequences, magnitudes, phases = signal.bode(system, w)
    frequences = (0.5/math.pi) * cycle_frequences

    # teoretical
    magnitudes_theoretical = [math.sqrt(1 + a**2 * w**2)/(1 - c * w**2) for w in frequences]
    phases_theoretical = [math.atan(a * w) for w in frequences]

    plt.figure()
    plt.semilogx(frequences, phases_theoretical)
    plt.semilogx(frequences, phases)
    plt.legend(['theoretical', 'experimental'])
    plt.title('ФЧХ. Вследствие неустойчивости системы теоретические\nзначения не совпадают с экспериментальными')
    plt.show()
    plt.figure()
    plt.semilogx(frequences, magnitudes_theoretical)
    plt.semilogx(frequences, magnitudes)
    plt.legend(['theoretical', 'experimental'])
    plt.title('АЧХ. Вследствие неустойчивости системы теоретические\nзначения не совпадают с экспериментальными')
    plt.show()

    t = np.arange(0, 10, 0.01)
    input_impulse = [0 if t_item < 0 else 1 for t_item in t]
    t_out, h_experimental, x_out = signal.lsim(system, input_impulse, t)

    h_theoretical = [1 + a / math.sqrt(c) * math.sin(t_item / math.sqrt(c)) -
         math.cos(t_item/math.sqrt(c)) for t_item in t]
    
    plt.figure()
    plt.plot(t, h_theoretical)
    plt.plot(t, h_experimental)
    plt.legend(['theoretical', 'experimental'])
    plt.title('Переходная функция\nГрафики совпадают')
    plt.show()