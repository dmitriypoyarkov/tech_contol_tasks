import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button

m = 0.5
I = 0.009
dt = 0.01
g = 9.8
total_time = 15

if __name__ == '__main__':
    a = 1
    transfer_function = [[I, 0, m * g * a], [1, 0]]

    A = np.array([[0, m * g * a / I], [1, 0]])
    B = np.array([[1/I],[0]])
    C = np.array([[0,1]])
    D = np.array([[0]])

    cond_equations = (A, B, C, D)

    t_in = np.linspace(0, total_time, int(total_time/dt) + 1)
    zero_input = np.zeros(t_in.shape) # не будем прилагать момент
    start_pos = - 15 / 180.0*np.pi

    t_out, y_out, x_out = signal.lsim(cond_equations, zero_input, t_in, X0=[0, start_pos])
    fig = plt.figure()
    plt.plot(t_out, y_out, 'b')
    plt.title('Система')
    limit = 0.1 / 180.0 * np.pi
    plt.plot([0, total_time], [limit, limit], 'r')
    plt.plot([0, total_time], [-limit, -limit], 'r')
    plt.show()

    # коэффициенты PID
    P = 4.95
    I_coef = 0
    D = 0.05
    feedback_transfer_function = [[1, 0], [I, D, P - m * g * a, I_coef]]
    feedback_sys = signal.tf2ss(*feedback_transfer_function)
    proportionality_coef = feedback_sys[2][0][1]
    print('Матрицы А, B, C, D для системы:\n', feedback_sys)
    t_out, y_out, x_out = signal.lsim(feedback_sys, zero_input, t_in, X0=[0, start_pos / proportionality_coef, 0])
    
    fig = plt.figure()
    plt.plot(t_out, y_out, 'b')
    plt.title(f'Система с PID: P={P}, I={I_coef}, D={D}')
    limit = 15 / 180.0 * np.pi
    plt.plot([0, total_time], [limit, limit], 'r')
    plt.plot([0, total_time], [-limit, -limit], 'r')
    plt.show()