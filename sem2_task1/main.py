# Фильтр Калмана

# Поярков Д. И.
# Вариант 8:

# 8.	Имеется квадрокоптер, летающий над ровным открытым
# пространством на одной высоте. От него мы регулярно
# получаем его географические координаты по данным двух
# независимых систем спутниковой навигации.
# Оценить положение квадрокоптера.

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button

def kalman_step(f_mat, z_prev, p_prev,
        h_mat, r_mat, y_cur, q_mat):
    z_cur = f_mat @ z_prev
    p_mat_cur = f_mat @ p_prev @ np.transpose(f_mat) + q_mat
    h_mat_t = np.transpose(h_mat)
    g_mat = p_mat_cur @ h_mat_t @ np.linalg.inv(h_mat @ p_mat_cur @ h_mat_t + r_mat)
    z_corrected = z_cur + g_mat @ (y_cur - h_mat @ z_cur)
    p_corrected = (np.eye(len(z_prev)) - g_mat @ h_mat) @ p_mat_cur
    return (z_corrected, p_corrected)

def kalman_filter(x0, p0_mat, y_seq,
        q_mat, h_mat, r_mat, f_mat):
    num_steps = len(y_seq)
    x_result = np.array([x0])
    p_mat_cur = p0_mat
    for i in range(1, num_steps):
        (x_cur, p_mat_cur) = kalman_step(f_mat, x_result[-1], p_mat_cur,
            h_mat, r_mat, y_seq[i - 1], q_mat)

        x_result = np.append(x_result, [x_cur], axis=0)
    return x_result


def test_model(x, y, dt, sigma_xy, q1, q2, q3, q4, q5, q6):
    vx = np.append((x[1] - x[0]) / dt, (x[1:] - x[:-1]) / dt)
    vy = np.append((y[1] - y[0]) / dt, (y[1:] - y[:-1]) / dt)

    ax = np.append((vx[1] - vx[0]) / dt, (vx[1:] - vx[:-1]) / dt)
    ay = np.append((vy[1] - vy[0]) / dt, (vy[1:] - vy[:-1]) / dt)

    # шумы
    x1_noised = x + np.random.normal(0, sigma_xy, x.shape)
    y1_noised = y + np.random.normal(0, sigma_xy, y.shape)
    x2_noised = x + np.random.normal(0, sigma_xy, x.shape)
    y2_noised = y + np.random.normal(0, sigma_xy, y.shape)

    # Y
    measurement_seq = np.array([[x1, y1, x2, y2]
        for (x1, y1, x2, y2)
        in zip(x1_noised, y1_noised, x2_noised, y2_noised)])
    h_mat = np.array([[1, 0, 0, 0, 0, 0],
                      [0, 1, 0, 0, 0, 0],
                      [1, 0, 0, 0, 0, 0],
                      [0, 1, 0, 0, 0, 0]])
    r_mat = np.diag(sigma_xy**2 * np.ones((4)))
    q_mat = np.diag([q1, q2, q3, q4, q5, q6])

    z0 = np.array([x[0], y[0], vx[0], vy[0], ax[0], ay[0]])
    p0_mat = 0.1 * np.eye(6)

    f_mat = np.array([[1, 0, dt, 0,  (dt**2)/2, 0        ],
                      [0, 1, 0,  dt, 0        , (dt**2)/2],
                      [0, 0, 1,  0,  dt       , 0        ],
                      [0, 0, 0,  1,  0        , dt       ],
                      [0, 0, 0,  0,  1        , 0        ],
                      [0, 0, 1,  0,  0        , 1        ]])
    

    z_filtered = kalman_filter(z0, p0_mat, measurement_seq,
        q_mat, h_mat, r_mat, f_mat)
    return z_filtered


if __name__ == "__main__":
    figure = plt.figure(figsize=(5.5, 7))
    
    time_steps = 100
    dt = 0.1
    # Пусть квадрокоптер тоже летает по кругу
    radius = 1
    split_nodes = np.linspace(0, time_steps * dt, time_steps)
    x = radius * np.cos(split_nodes)
    y = radius * np.sin(split_nodes)

    z_filtered = test_model(x, y, dt, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1)
    plt.title("Robot positions")
    plt.xlabel("x")
    plt.ylabel("y")
    # точное положение
    plt.plot(x, y, "r")
    plt.subplots_adjust(top=0.65)
    # оценка по показаниям приборов
    line, = plt.plot([z[1] for z in z_filtered],
                     [z[0] for z in z_filtered], "g")

    axfreq = plt.axes([0.1, 0.95, 0.65, 0.03])
    sigma_slider = Slider(
        ax=axfreq,
        label='Noise',
        valmin=0,
        valmax=0.99,
        valinit=0.1,
    )
    q_mat_sliders = [Slider(
        ax=plt.axes([0.1, 0.95 - 0.04 * i, 0.65, 0.03]),
        label=f'q{i}',
        valmin=0,
        valmax=20,
        valinit=0.1,
    ) for i in range(1, 7)]

    def update_q(val, arg_num):
        cur_args = [slider.val for slider in q_mat_sliders]
        cur_args[arg_num] = val
        z_filtered = test_model(x, y, dt, sigma_slider.val, *cur_args)
        line.set_data([z[1] for z in z_filtered],
            [z[0] for z in z_filtered])
        figure.canvas.draw_idle()

    update_functions = [lambda val : update_q(val, i) for i in range(0, 6)]
    for i, slider in enumerate(q_mat_sliders):
        slider.on_changed(update_functions[i])

    def update_sigma_xy(val):
        args = [slider.val for slider in q_mat_sliders]
        z_filtered = test_model(x, y, dt, val, *args)
        line.set_data([z[1] for z in z_filtered],
            [z[0] for z in z_filtered])
        figure.canvas.draw_idle()

    sigma_slider.on_changed(update_sigma_xy)
    plt.show()