# Фильтр Частиц

# Поярков Д. И.
# Вариант 8:

# 8. Имеется квадрокоптер, летающий над ровным открытым
# пространством на одной высоте. От него мы регулярно
# получаем его географические координаты по данным двух
# независимых систем спутниковой навигации.
# Оценить положение квадрокоптера.

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button

import numba as nb
from numba import njit, jit

@njit
def gaussian(x, mean, dev):
    """ рассчитать функцию нормального распределения от x 
    со средним значением mean и среднеквадратическим отклонением dev """
    return np.array(np.exp(-0.5 * (x - mean)**2 / dev**2) / (dev * (2 * np.pi)**0.5),
        dtype = np.dtype(np.float64))

@njit
def resampling(weights, points):
    NP = weights.shape[0]
    curr_level = np.array(np.random.random() / NP, dtype=np.dtype(np.float64))
    level_diff = np.array(1. / NP, dtype=np.dtype(np.float64))
    chosen_points = np.empty(NP, dtype='i8')
    current_index = np.array(0, dtype='i8')
    current_sum = weights[0]
    for check_point in range(NP):
        while current_sum < curr_level:
            current_sum += weights[current_index+1]
            current_index += np.array(1, dtype='i8')
        chosen_points[check_point] = current_index
        curr_level += level_diff
    
    res = np.empty(points.shape)
    for i in range(chosen_points.shape[0]):
        res[i] = points[chosen_points[i]]
        
    return res

@njit
def pf_step(Z0s, Y, H, dt, scales, random_shifts_devs, f_mat):
    n_vars = Z0s.shape[1]
    new_Zs = np.empty(Z0s.shape, dtype=np.dtype(np.float64))
    weights = np.empty(Z0s.shape[0], dtype=np.dtype(np.float64))
    # для каждой частицы
    for i in range(Z0s.shape[0]):
        new_Z = f_mat @ Z0s[i]
        Y_est = H @ new_Z
        dist = np.array(
            scales[0] * (((Y[0] - Y_est[0])**2 + (Y[1] - Y_est[1])**2)**0.5 + 
            ((Y[2] - Y_est[2])**2 + (Y[3] - Y_est[3])**2)**0.5),
            dtype=np.dtype(np.float64)
        ) 
        weight = gaussian(dist, 0, 2)
        new_Zs[i] = new_Z
        weights[i] = weight
    sw = np.sum(weights)
    weights /= sw
    Z_weighted = new_Zs * weights.reshape(Z0s.shape[0], 1)
    Z = np.sum(Z_weighted, 0)
    new_Z0s = resampling(weights, new_Zs)
    new_Z0s_randomized = np.empty(new_Z0s.shape)
    for i in range(new_Z0s.shape[0]):
        new_Z0s_randomized[i] = new_Z0s[i] + np.random.randn(n_vars) * random_shifts_devs
    return (Z, new_Z0s_randomized)


def generate_particles(limits, p_num):
    p_length = limits.shape[0]
    mins = np.array([[x[0] for x in limits]])
    maxs = np.array([[x[1] for x in limits]])
    res = np.empty((p_num, p_length), dtype=np.dtype(np.float64))
    for i in range(res.shape[0]):
        v = np.random.rand(p_length)
        res[i] = mins * v + maxs * (1 - v)
    return res


def pf(Z0, Z_limits, pnum, Y, H, dt, scales, random_shifts_devs, f_mat):
    num_steps = Y.shape[0]
    Z_res = np.array([Z0])
    Zs = generate_particles(Z_limits, pnum)
    for i in range(1, num_steps):
        (Z, Zs) = pf_step(Zs, Y[i], H, dt, scales, random_shifts_devs, f_mat)
        Z_res = np.append(Z_res, [Z], axis=0)
    return Z_res


def test_model(x, y, dt, sigma_xy, s1, s2, s3, s4):

    vx = np.append((x[1:] - x[:-1]) / dt, (x[-1] - x[-2]) / dt).astype(np.dtype(np.float64))
    vy = np.append((y[1:] - y[:-1]) / dt, (y[-1] - y[-2]) / dt).astype(np.dtype(np.float64))

    ax = np.append((vx[1] - vx[0]) / dt, (vx[1:] - vx[:-1]) / dt).astype(np.dtype(np.float64))
    ay = np.append((vy[1] - vy[0]) / dt, (vy[1:] - vy[:-1]) / dt).astype(np.dtype(np.float64))

    # шумы
    y1_noised = (y + np.random.normal(0, sigma_xy, y.shape)).astype(np.dtype(np.float64))
    x1_noised = (x + np.random.normal(0, sigma_xy, x.shape)).astype(np.dtype(np.float64))
    x2_noised = (x + np.random.normal(0, sigma_xy, x.shape)).astype(np.dtype(np.float64))
    y2_noised = (y + np.random.normal(0, sigma_xy, y.shape)).astype(np.dtype(np.float64))

    # Y
    measurement_seq = np.array(
        [[x1, y1, x2, y2]
        for x1, y1, x2, y2
        in np.dstack([x1_noised, y1_noised, x2_noised, y2_noised])[0]],
        dtype=np.dtype(np.float64)
    )
    h_mat = np.array([[1, 0, 0, 0, 0, 0],
                      [0, 1, 0, 0, 0, 0],
                      [1, 0, 0, 0, 0, 0],
                      [0, 1, 0, 0, 0, 0]], dtype=np.dtype(np.float64))

    z0 = np.array([x[0], y[0], vx[0], vy[0], ax[0], ay[0]], dtype=np.dtype(np.float64))

    f_mat = np.array([[1, 0, dt, 0,  (dt**2)/2, 0        ],
                      [0, 1, 0,  dt, 0        , (dt**2)/2],
                      [0, 0, 1,  0,  dt       , 0        ],
                      [0, 0, 0,  1,  0        , dt       ],
                      [0, 0, 0,  0,  1        , 0        ],
                      [0, 0, 0,  0,  0        , 1        ]], dtype=np.dtype(np.float64))

    p_num = 1000
    scales = np.array([s1, s2, s3, s4], dtype=np.dtype(np.float64))
    random_shifts_devs = np.array([1., 1., 1., 1., 1., 1.], dtype=np.dtype(np.float64))
    z_filtered = pf(z0, Z_limits, p_num, measurement_seq, h_mat, dt, scales, random_shifts_devs, f_mat)
    return z_filtered, x1_noised, y1_noised


if __name__ == "__main__":
    figure = plt.figure(figsize=(5.5, 7))
    
    time_steps = 65
    dt = 0.1
    # Пусть квадрокоптер тоже летает по кругу
    radius = 1
    split_nodes = np.linspace(0, time_steps * dt, time_steps)
    x = radius * np.cos(split_nodes)
    y = radius * np.sin(split_nodes)

    Z_limits = np.array([
        [radius - 0.1, radius + 0.1],   # x
        [-0.1, 0.1],                    # y
        [-0.5, 0.5],                    # vx
        [0.0, 2],                  # vy
        [-0.02, 0.02],                  # ax
        [-0.02, 0.02],                  # ay
    ])
    
    z_filtered, x1_noised, y1_noised = test_model(x, y, dt, 0.1, 1, 1, 1, 1)
    plt.title("Robot positions")
    plt.xlabel("x")
    plt.ylabel("y")
    # точное положение
    xy_line, = plt.plot(x1_noised, y1_noised, "r", linewidth=0.5)
    plt.subplots_adjust(top=0.65)
    
    # оценка по показаниям приборов
    line, = plt.plot(np.array([z[0] for z in z_filtered]),
                     np.array([z[1] for z in z_filtered]), "g")
    xy = np.array(list(zip(x, y)))
    deviation = (np.sum([(xy[i] - z_filtered[i][:2])**2 for i in range(xy.shape[0])]) / xy.shape[0])**0.5
    maxdev = max([np.sum((xy[i] - z_filtered[i][:2])**2)**0.5 for i in range(xy.shape[0])])
    deviation_text = plt.text(-0.5, 0.1, f'stdev: {np.round(deviation, decimals=2)}')
    maxdev_text = plt.text(-0.5, 0.2, f'maxdev: {np.round(maxdev, decimals=2)}')

    # plt.legend(['noised data', 'filtered data'])
    axfreq = plt.axes([0.1, 0.95, 0.65, 0.03])
    sigma_slider = Slider(
        ax=axfreq,
        label='Noise',
        valmin=0,
        valmax=0.5,
        valinit=0.1,
    )
    s_slider = Slider(
        ax=plt.axes([0.1, 0.95 - 0.04, 0.65, 0.03]),
        label=f's',
        valmin=0,
        valmax=8,
        valinit=1,
    )

    def update_s(val):
        z_filtered, x_noised, y_noised = test_model(x, y, dt, sigma_slider.val, *([val]*4))
        line.set_data([z[0] for z in z_filtered],
            [z[1] for z in z_filtered])
        xy_line.set_data(x, y)
        deviation = (np.sum([(xy[i] - z_filtered[i][:2])**2 for i in range(xy.shape[0])]) / xy.shape[0])**0.5
        maxdev = max([np.sum((xy[i] - z_filtered[i][:2])**2)**0.5 for i in range(xy.shape[0])])
        deviation_text.set_text(f'stdev: {np.round(deviation, decimals=2)}')
        maxdev_text.set_text(f'maxdev: {np.round(maxdev, decimals=2)}')
        figure.canvas.draw_idle()

    s_slider.on_changed(update_s)

    def update_sigma_xy(val):
        z_filtered, x_noised, y_noised = test_model(x, y, dt, val, *([s_slider.val]*4))
        line.set_data([z[0] for z in z_filtered],
            [z[1] for z in z_filtered])
        xy_line.set_data(x, y)
        deviation = (np.sum([(xy[i] - z_filtered[i][:2])**2 for i in range(xy.shape[0])]) / xy.shape[0])**0.5
        maxdev = max([np.sum((xy[i] - z_filtered[i][:2])**2)**0.5 for i in range(xy.shape[0])])
        deviation_text.set_text(f'stdev: {np.round(deviation, decimals=2)}')
        maxdev_text.set_text(f'maxdev: {np.round(maxdev, decimals=2)}')
        figure.canvas.draw_idle()

    sigma_slider.on_changed(update_sigma_xy)
    plt.show()