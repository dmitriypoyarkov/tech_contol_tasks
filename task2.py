from matplotlib.lines import Line2D
from numpy.lib.type_check import real
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import math
from matplotlib.animation import FuncAnimation


if __name__ == '__main__':
    # feedback system
    plt.figure()
    plt.text(0.15,0.5,'Система без обратной связи', fontsize = 'xx-large')
    plt.show()

    a = 1
    pole1 = []
    pole2 = []
    param_list = np.exp(np.arange(0, 10, 0.01))
    for c in param_list:
        transmission_function = [[a, 1], [c, 0, 1]]
        z, p, k = signal.tf2zpk(*transmission_function)
        pole1.append(p[0])
        pole2.append(p[1])
    real_parts = [[point.real for point in pole1], [point.real for point in pole2]]
    imaginary_parts = [[point.imag for point in pole1], [point.imag for point in pole2]]
    
    for i in range(2):
        plt.figure()
        plt.plot(param_list, imaginary_parts[i])
        plt.ylabel('Im(p)')
        plt.xlabel('c')
        plt.title(f'Зависимость мнимой части от параметра (полюс {i+1})')

        plt.figure()
        plt.plot(param_list, real_parts[i])
        plt.ylabel('Re(p)')
        plt.xlabel('c')
        plt.title(f'Вещественная часть всегда нулевая (полюс {i+1})')
        plt.show()

    a = 1
    c = 2
    transmission_function = [[a, 1], [c, 0, 1]]
    system = signal.lti(*transmission_function)
    t = np.arange(0, 200, 0.1)
    finite_signal = [1 if t_item > 20 and t_item < 60 else 0 for t_item in t]
    t_out, y_out, x_out = signal.lsim(system, finite_signal, t)
    plt.figure()
    plt.plot(t, finite_signal)
    plt.plot(t, y_out)
    plt.legend(['input', 'output'])
    plt.xlabel('t')
    plt.ylabel('signal')
    plt.title('При подаче конечного воздействия система не затухает, но и не\nуходит на бесконечность (система неустойчива при любом с)')
    plt.show()


    # experimental
    w = np.arange(0, 10**2, 0.01)
    cycle_frequences, magnitudes, phases = signal.bode(system, w)
    frequences = (0.5/math.pi) * cycle_frequences
    plt.figure()
    plt.semilogx(frequences, phases)
    plt.title('ФЧХ. Система неустойчива, следовательно,\nзависимость не имеет смысла')
    plt.figure()
    plt.semilogx(frequences, magnitudes)
    plt.title('АЧХ. Система неустойчива, следовательно,\nзависимость не имеет смысла')
    plt.show()
#########################################


    # feedback system
    plt.figure()
    plt.text(0.05,0.5,'Далее: Система с обратной связью', fontsize = 'xx-large')
    plt.show()

    a = 2
    poles = [[],[],[],[]]
    param_list = np.logspace(20, 100, 20000)
    param_list = np.linspace(-100, 100, 5000)
    for c in param_list:
        transmission_function = [[a*c, c, a, 1], [c**2, 0, 2*c+a**2, 2*a, 2]]
        z, p, k = signal.tf2zpk(*transmission_function)
        for i, pole in enumerate(poles):
            pole.append(p[i])
    real_parts = [[point.real for point in pole] for pole in poles]
    imaginary_parts = [[point.imag for point in pole] for pole in poles]
    
    for i in range(len(poles)):
        plt.figure()
        plt.ylabel('Im')
        plt.xlabel('Re')
        plt.title(f'Траектория полюса на комплексной плоскости (полюс {i+1})')
        
        offset = 4 if i <= 2 else 0

        start_point = [real_parts[i][0], imaginary_parts[i][0]]
        end_point = [real_parts[i][len(real_parts[i]) - 1], imaginary_parts[i][len(imaginary_parts[i]) - 1]]
        plt.text(start_point[0], start_point[1], f'c = {param_list[0]}')
        plt.text(end_point[0], end_point[1] - offset, f'c = {param_list[len(param_list) - 1]}')
        plt.scatter(real_parts[i], imaginary_parts[i], 0.5)
    plt.show()

    a = 1
    c = 1
    transmission_function = [[a*c, c, a, 1], [c**2, 0, 2*c+a**2, 2*a, 2]]
    system = signal.lti(*transmission_function)
    t = np.arange(0, 15, 0.1)
    finite_signal = [1 if t_item > 5 and t_item < 10 else 0 for t_item in t]
    t_out, y_out, x_out = signal.lsim(system, finite_signal, t)
    plt.figure()
    plt.plot(t, finite_signal)
    plt.plot(t, y_out)
    plt.legend(['input', 'output'])
    plt.xlabel('t')
    plt.ylabel('signal')
    plt.title(f'Система неустойчива.\nТочка a = {a}, c = {c}: система нарастает')
    plt.show()

    # experimental
    w = np.arange(0, 10**2, 0.01)
    cycle_frequences, magnitudes, phases = signal.bode(system, w)
    frequences = (0.5/math.pi) * cycle_frequences
    plt.figure()
    plt.semilogx(frequences, phases)
    plt.title('ФЧХ. Система неустойчива, следовательно,\nзависимость не имеет смысла')
    plt.figure()
    plt.semilogx(frequences, magnitudes)
    plt.title('АЧХ. Система неустойчива, следовательно,\nзависимость не имеет смысла')
    plt.show()